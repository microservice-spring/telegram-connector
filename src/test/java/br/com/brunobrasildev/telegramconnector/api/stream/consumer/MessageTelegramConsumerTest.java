package br.com.brunobrasildev.telegramconnector.api.stream.consumer;

import br.com.brunobrasildev.telegramconnector.api.stream.producer.MessageTelegramReturnProducer;
import br.com.brunobrasildev.telegramconnector.data.model.Message;
import br.com.brunobrasildev.telegramconnector.data.model.MessageReturn;
import br.com.brunobrasildev.telegramconnector.core.service.MessageService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.support.Acknowledgment;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MessageTelegramConsumerTest {

    private static final String MSN_RETURN = "Message Telegram sent success.";
    private static final String TEXT_NULL = "Text message is null.";
    private static final String TEXT = "Hello World";

    @InjectMocks
    private MessageTelegramConsumer consumer;

    @Mock
    private MessageService service;

    @Mock
    private MessageTelegramReturnProducer producer;

    @Test
    void givenMessageEvent_whenCallConsumer_thenSendSuccess() {
        Message message = Message.builder()
                .text(TEXT)
                .build();

        MessageReturn messageReturn = MessageReturn.builder()
                .success(true)
                .msn(MSN_RETURN)
                .build();

        when(service.send(message)).thenReturn(messageReturn);

        consumer.consumer(message);

        verify(service).send(message);
        verify(producer).notifyReturn(messageReturn);
    }

    @Test
    void givenTextNullEvent_whenCallConsumer_thenValidate() {
        Message message = Message.builder()
                .build();

        when(service.send(message)).thenThrow(new IllegalArgumentException(TEXT_NULL));

        consumer.consumer(message);

        verifyNoInteractions(producer);
    }

    @Test
    void givenNullEvent_whenCallConsumer_thenValidate() {
        consumer.consumer(null);

        verifyNoInteractions(service);
        verifyNoInteractions(producer);
    }

}
