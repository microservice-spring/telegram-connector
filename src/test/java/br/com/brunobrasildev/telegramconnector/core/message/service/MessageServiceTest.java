package br.com.brunobrasildev.telegramconnector.core.message.service;

import br.com.brunobrasildev.telegramconnector.core.service.MessageService;
import br.com.brunobrasildev.telegramconnector.data.model.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
public class MessageServiceTest {

    private static final String TEXT = "Hello World";
    private static final String URL_TELEGRAM = "http://example.com";
    private static final String API_TOKEN = "23423:23ihrt234rjb4tk4";
    private static final String CHAT_ID = "5464644";

    @InjectMocks
    private MessageService messageService;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(messageService, "url", URL_TELEGRAM);
        ReflectionTestUtils.setField(messageService, "apiToken", API_TOKEN);
        ReflectionTestUtils.setField(messageService, "chatId", CHAT_ID);
    }

    @Test
    void givenMessage_whenCallSend_thenSentSuccess() throws Exception {
        Message message = Message.builder()
                .text(TEXT)
                .build();

        messageService.send(message);
    }


}
