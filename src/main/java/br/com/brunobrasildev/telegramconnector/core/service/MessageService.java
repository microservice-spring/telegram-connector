package br.com.brunobrasildev.telegramconnector.core.service;

import br.com.brunobrasildev.telegramconnector.data.model.Message;
import br.com.brunobrasildev.telegramconnector.data.model.MessageReturn;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

@Service
@Slf4j
public class MessageService {

    @Value("${telegram.url}")
    private String url;

    @Value("${telegram.apiToken}")
    private String apiToken;

    @Value("${telegram.chatId}")
    private String chatId;

    public MessageReturn send(Message message) {
        Assert.notNull(message.getText(), "Text message is null.");

        String urlSend = url + "/bot%s/sendMessage?chat_id=%s&text=%s";

        urlSend = String.format(urlSend, apiToken, chatId, message.getText());

        try {
            URL url = new URL(urlSend);
            URLConnection conn = url.openConnection();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            log.info("Telegram sent message.");
            return MessageReturn.builder()
                    .success(true)
                    .msn("Message Telegram sent success.")
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Error: {} send message Telegram.", e.getMessage());
            return MessageReturn.builder()
                    .success(false)
                    .msn(e.getMessage())
                    .build();
        }
    }

}
