package br.com.brunobrasildev.telegramconnector.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class MessageReturn {

    private Boolean success;
    private String msn;

}
