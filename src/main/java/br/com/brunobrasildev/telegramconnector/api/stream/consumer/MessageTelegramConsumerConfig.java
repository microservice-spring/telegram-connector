package br.com.brunobrasildev.telegramconnector.api.stream.consumer;

import br.com.brunobrasildev.telegramconnector.data.model.Message;
import br.com.brunobrasilweb.kafkacore.KafkaAbstractConsumer;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;

@Configuration
@AllArgsConstructor
public class MessageTelegramConsumerConfig extends KafkaAbstractConsumer<Message> {

    public static final String CONTAINER_FACTORY_BEAN_NAME = "messageContainerFactory";

    @Bean(CONTAINER_FACTORY_BEAN_NAME)
    public ConcurrentKafkaListenerContainerFactory<String, Message> messageContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, Message>
                factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(buildConsumerFactory(Message.class));
        return factory;
    }

}
