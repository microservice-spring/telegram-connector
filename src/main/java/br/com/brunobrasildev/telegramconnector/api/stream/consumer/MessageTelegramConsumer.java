package br.com.brunobrasildev.telegramconnector.api.stream.consumer;

import br.com.brunobrasildev.telegramconnector.api.stream.producer.MessageTelegramReturnProducer;
import br.com.brunobrasildev.telegramconnector.data.model.Message;
import br.com.brunobrasildev.telegramconnector.data.model.MessageReturn;
import br.com.brunobrasildev.telegramconnector.core.service.MessageService;
import br.com.brunobrasilweb.kafkacore.KafkaConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import static br.com.brunobrasildev.telegramconnector.api.stream.consumer.MessageTelegramConsumerConfig.CONTAINER_FACTORY_BEAN_NAME;
import static br.com.brunobrasildev.telegramconnector.core.config.KafkaTopicConfig.SEND_MESSAGE_TELEGRAM_TOPIC;

@Component
@Slf4j
@AllArgsConstructor
public class MessageTelegramConsumer {

    private MessageService service;
    private MessageTelegramReturnProducer producer;

    @KafkaListener(topics = SEND_MESSAGE_TELEGRAM_TOPIC, containerFactory = CONTAINER_FACTORY_BEAN_NAME)
    public void consumer(Message event) {
        log.info("Event received. Topic: [{}], Event: [{}].", SEND_MESSAGE_TELEGRAM_TOPIC, event);

        try {
            if (event != null) {
                MessageReturn messageReturn = service.send(event);

                producer.notifyReturn(messageReturn);
            } else {
                log.info("Event received is null.");
            }
        } catch (Exception e) {
            log.info("Event send error [{}].", e.getMessage());
        }
    }

}
