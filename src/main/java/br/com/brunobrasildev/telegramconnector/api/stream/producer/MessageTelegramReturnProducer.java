package br.com.brunobrasildev.telegramconnector.api.stream.producer;

import br.com.brunobrasildev.telegramconnector.data.model.MessageReturn;
import br.com.brunobrasilweb.kafkacore.KafkaAbstractProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static br.com.brunobrasildev.telegramconnector.core.config.KafkaTopicConfig.SEND_MESSAGE_TELEGRAM_RETURN_TOPIC;

@Component
@Slf4j
public class MessageTelegramReturnProducer extends KafkaAbstractProducer {

    public void notifyReturn(MessageReturn event) {
        sendMessage(SEND_MESSAGE_TELEGRAM_RETURN_TOPIC, event);

        log.info("Event sent. Topic: [{}], Event: [{}].", SEND_MESSAGE_TELEGRAM_RETURN_TOPIC, event);
    }

}
